import { Action } from "redux";

export interface IPerson {
    fullName: string;
    email: string;
    age: number;
}

const initialState: IPerson[] = 
[
    {
        fullName: 'Name Surname1',
        email: 'mail1@mail.mail',
        age: 1,
    },
    {
        fullName: 'Name Surname2',
        email: 'mail2@mail.mail',
        age: 2,
    },
    {
        fullName: 'Name Surname3',
        email: 'mail3@mail.mail',
        age: 3,
    },
    {
        fullName: 'Name Surname4',
        email: 'mail4@mail.mail',
        age: 4,
    }
];

export type IPersonsStore = IPerson[];

export const stateReducer =
(
    state: IPersonsStore = initialState,
    action: Action,
) => {
    switch (action.type) {
        default: return state;
    }
}