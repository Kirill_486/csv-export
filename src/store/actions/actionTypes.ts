const statePrefix = 'STATE_';
const sagaPrefix = 'SAGA_';

export const stateActionTypes = Object.freeze({
    SET: `${statePrefix}SET`
});

export const sagaActionTypes = Object.freeze({
    EXPORTCSV: `${sagaPrefix}EXPORTCSV`
});