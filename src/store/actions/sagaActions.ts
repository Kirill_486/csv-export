import { Action, ActionCreator } from 'redux';
import { sagaActionTypes } from './actionTypes';

export const exportCsv: ActionCreator<Action> =
() => {
    return {
        type: sagaActionTypes.EXPORTCSV
    }
}