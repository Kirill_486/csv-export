import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from '../sagas/rootSaga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { stateReducer } from './reducers/stateReducer';

const sagaMiddleware = createSagaMiddleware();

const enhanser = composeWithDevTools(applyMiddleware(sagaMiddleware));

const store = createStore(stateReducer, enhanser);

sagaMiddleware.run(rootSaga, {
    type: 'none'
});

export {store};
