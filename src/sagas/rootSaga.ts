import { call, takeEvery, select } from '@redux-saga/core/effects';
import { Action } from 'redux';
import {sagaActionTypes} from '../store/actions/actionTypes';
import { IPerson } from 'src/store/reducers/stateReducer';

export function* rootSaga(action: Action) {
    yield call(console.log, 'it just works');
    yield takeEvery(sagaActionTypes.EXPORTCSV, exportCsvSaga)
}

function* exportCsvSaga(action: Action) {
    yield call(console.log, 'export to csv');
    const state = yield select();
    // yield call(console.log, state);
    
    const headers = ['name', 'email', 'age'].join(',');
    const rows = state.map((item: IPerson): string => `${item.fullName},${item.email},${item.age}`);

    const data = [headers, ...rows].join('\n');
    yield call(console.log, data);

    const anker = document.createElement('a');
    anker.href = `data:arrachement/csv,${data}`;
    anker.target = '_Blank';
    anker.download = 'schedule.csv';
    document.body.appendChild(anker);

    anker.click();
}