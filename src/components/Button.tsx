import * as React from 'react';

export interface IButtonStateProps {
    title: string;
}

export interface IButtonDispatchProps {
    onClick: () => void;
}

export interface IButtonProps extends
IButtonStateProps,
IButtonDispatchProps
{ }

export const Button: React.SFC<IButtonProps> = (props) => {
    return (
        <button
            className="button-standart"
            onClick={props.onClick}
        >{props.title}</button>
    );
}

Button.defaultProps = {
    onClick: () => console.log('button did nothing by default'),
    title: 'defaultTitle',
}

Button.displayName = 'StandartButton';