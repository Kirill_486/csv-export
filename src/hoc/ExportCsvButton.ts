import { connect } from 'react-redux';
import { Button, IButtonStateProps, IButtonDispatchProps } from 'src/components/Button';
import { exportCsv } from 'src/store/actions/sagaActions';

const mapStateToProps =
(): IButtonStateProps => {
    return {
        title: 'Export to CSV'
    }
}

const mapDispatchToProps =
(
    dispatch: any
): IButtonDispatchProps => {
    return {
        onClick: () => dispatch(exportCsv()) 
    }
}

export const ExportCSVButton = connect(mapStateToProps, mapDispatchToProps)(Button);